package native

import (
	"fmt"
	"gitlab.com/Raven-IO/raven-delve/pkg/proc"
)

func (t *nativeThread) restoreRegisters(savedRegs proc.Registers) error {
	return fmt.Errorf("restore regs not supported on i386")
}
