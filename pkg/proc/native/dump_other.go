//go:build darwin || (windows && arm64)

package native

import (
	"gitlab.com/Raven-IO/raven-delve/pkg/elfwriter"
	"gitlab.com/Raven-IO/raven-delve/pkg/proc"
)

func (p *nativeProcess) MemoryMap() ([]proc.MemoryMapEntry, error) {
	return nil, proc.ErrMemoryMapNotSupported
}

func (p *nativeProcess) DumpProcessNotes(notes []elfwriter.Note, threadDone func()) (threadsDone bool, notesout []elfwriter.Note, err error) {
	return false, notes, nil
}
