package main

import (
	"os"

	"github.com/sirupsen/logrus"
	"gitlab.com/Raven-IO/raven-delve/cmd/dlv/cmds"
	"gitlab.com/Raven-IO/raven-delve/pkg/version"
)

// Build is the git sha of this binaries build.
var Build string

func main() {
	if Build != "" {
		version.DelveVersion.Build = Build
	}

	const cgoCflagsEnv = "CGO_CFLAGS"
	if os.Getenv(cgoCflagsEnv) == "" {
		os.Setenv(cgoCflagsEnv, "-O0 -g")
	} else {
		logrus.WithFields(logrus.Fields{"layer": "dlv"}).Warnln("CGO_CFLAGS already set, Cgo code could be optimized.")
	}

	cmds.New(false).Execute()
}
