package main

import (
	"fmt"
	"gitlab.com/Raven-IO/raven-delve/_fixtures/internal/dir.io"
	"gitlab.com/Raven-IO/raven-delve/_fixtures/internal/dir.io/io.io"
	"runtime"
)

func main() {
	var iface interface{} = &dirio.SomeType{}
	var iface2 interface{} = &ioio.SomeOtherType{}
	runtime.Breakpoint()
	fmt.Println(iface, iface2)
}
